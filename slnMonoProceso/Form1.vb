﻿Imports System.IO
Imports System.Text

Public Class Form1

    Dim objTarea As New clsTarea
    Dim objCorreo As New clsCorreo
    Dim objSQL As New clsOperacionesSQL


    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Try
            'MsgBox("Sincronizando")

            Sincronizacion()
            'If My.Settings.EjecucionManual = 1 Then
            '    'conexionODBC()
            'End If

        Catch ex As Exception
            respuestasAplicativo(ex.Message)
        End Try
        Close()

    End Sub

    Private Sub btnEjecutar_Click(sender As Object, e As EventArgs) Handles btnEjecutar.Click

    End Sub

    Public Sub respuestasAplicativo(ByVal respuesta As String)

        If My.Settings.Respuestas_X_Pantalla = 1 Then
            txtResultado.Text = respuesta
        Else

        End If

    End Sub


    Private Sub Sincronizacion()

        Try
            Dim dsOrigen As DataSet
            objTarea.BaseDatos = "SQL"
            Dim Plano As String
            Dim Resultado As String = ""
            Dim idTareaValido As Boolean = True
            Dim PlanoGenerado As String



            'Id de la tarea enviado como parametro
            Dim args As String() = Environment.GetCommandLineArgs()


            'aca evaluo si es parametro 6 ejecuta  el envio del correo 

            If args.GetValue(1) = 6 Then
                EnvioCorreo()
            ElseIf args.GetValue(1) = 7 Then
                EnvioCorreoOC()




            Else
                'MsgBox(args.ToString())

                If args.Length = 2 Then
                    If IsNumeric(args(1)) Then
                        objTarea.Tarea = args(1)
                        objTarea.LogPrincipalAlmacenar()
                        objTarea.LogInicio()
                    Else
                        objTarea.LogRecuperacionDatosOrigen(0)
                        objTarea.LogMensajesError("El argumento enviado debe ser numérico, verifique la configuración de la tarea programada y configure como argumento el Id de la tarea")
                        objCorreo.EnviarCorreoTarea("Validaciones", objTarea.CorreosNotificaciones, "El argumento enviado debe ser numérico, verifique la configuración de la tarea programada y configure como argumento el Id de la tarea")
                        Exit Sub
                    End If
                Else
                    objTarea.LogRecuperacionDatosOrigen(0)
                    objTarea.LogMensajesError("No se envió el Id de la tarea como argumento al programa, verifique la configuración de la tarea programada y configure como argumento el Id de la tarea")
                    objCorreo.EnviarCorreoTarea("Validaciones", objTarea.CorreosNotificaciones, "No se envió el Id de la tarea como argumento al programa, verifique la configuración de la tarea programada y configure como argumento el Id de la tarea")
                    Exit Sub
                End If

                'Obtener los datos fuentes del cliente para la tarea X
                Try
                    objTarea.LogFechaInicioRecuperacionDatosOrigen()

                    dsOrigen = objTarea.DatosOrigen(idTareaValido)

                    'MsgBox("Va a consumir")

                    If idTareaValido Then
                        objTarea.LogFechaFinRecuperacionDatosOrigen()
                        objTarea.LogRecuperacionDatosOrigen(1)
                    Else
                        objTarea.LogRecuperacionDatosOrigen(0)
                        objTarea.LogMensajesError("No se encontró el Id de tarea: " & objTarea.Tarea & " en la base de datos, verifique la configuración de la tarea programada de Windows")
                        objCorreo.EnviarCorreoTarea("Obtencion datos fuentes de la tarea", objTarea.CorreosNotificaciones, "No se encontró el Id de tarea: " & objTarea.Tarea & " en la base de datos, verifique la configuración de la tarea programada de Windows")
                        Exit Sub
                    End If
                Catch ex As Exception
                    objTarea.LogRecuperacionDatosOrigen(0)
                    objTarea.LogMensajesError(ex.Message)
                    objCorreo.EnviarCorreoTarea("Exception - Obtencion datos fuentes de la tarea", objTarea.CorreosNotificaciones, ex.Message)
                    Exit Sub
                End Try


                'Invocar Web Service de GT para Obtener el plano o realizar la importacion
                Try

                    objTarea.LogFechaInicioGeneracionPlano()
                    Dim objGenericTransfer As New wsGT1.wsGenerarPlano

                    objGenericTransfer.Timeout = 999999
                    Dim result As String = ""

                    'MsgBox("va a enviar")

                    If dsOrigen.Tables.Count >= 1 Then
                        If dsOrigen.Tables(0).Rows.Count >= 1 Then
                            'MsgBox("va a enviar 1")

                            result = objGenericTransfer.ImportarDatosDS(objTarea.IdDocumento,
                                                            objTarea.NombreDocumento,
                                                            2, "1", "gt", "gt",
                                                            dsOrigen, objTarea.RutaGeneracionPlano)
                            'MsgBox("va a enviar2", result.ToString())



                            'MsgBox(Plano)
                            'MsgBox(objTarea.EnviarNotificaciones & "-" & objTarea.CorreosNotificaciones)


                            If result <> "Importacion exitosa" Then
                                objCorreo.EnviarCorreoTarea("Exception - Consumo WS GTIntegration", objTarea.CorreosNotificaciones, result)
                            Else
                                objTarea.AlmacenarLogOC()
                            End If


                        End If
                    End If



                    'MsgBox("Ya envio")


                    objTarea.LogFechaFinGeneracionPlano()
                    objTarea.LogGeneracionDePlano(1)

                    If result <> "Importacion exitosa" And result <> "" Then
                        almacenarLogErrores(result)


                        If objTarea.EnviarNotificaciones = "True" Then

                            objCorreo.EnviarCorreoTarea("Resultado Consumo GTIntegration", objTarea.CorreosNotificaciones, Plano.ToString)

                        End If
                    End If

                Catch ex As Exception
                    objTarea.LogGeneracionDePlano(0)
                    objTarea.LogMensajesError(ex.InnerException.Message)
                    'objCorreo.EnviarCorreoTarea("Exception - Consumo WS GTIntegration", objTarea.CorreosNotificaciones, ex.InnerException.Message)
                    Exit Sub
                End Try


                'AlmacenarXML(PlanoGenerado.ToString.Replace(".txt", ".xml"), XMLCreacionTercero)

                objTarea.LogEjecucionCompleta()
                objTarea.LogFechaFin()
                objTarea.LogFin(1)

            End If
        Catch ex As Exception
            MsgBox(ex.Message & "Error Excepcion")
        End Try
        'Invocar Web Service UNOEE
        ''InvocarWebServiceUNOEE(Plano)

    End Sub

    Private Sub EnvioCorreoOC()
        Dim datos As New DataSet
        'Dim config As New DataSet
        Try
            'MsgBox("Entro a enviar correo")
            objTarea.SendMailOC(datos)
            'MsgBox("consulto bn")

            Dim MensajeCorreo As New StringBuilder

            If datos.Tables(0).Rows.Count > 0 Then
                For Each Row As DataRow In datos.Tables(0).Rows

                    MensajeCorreo.AppendFormat("<h1>Notificación: Orden de Compra Aliados</h1>")
                    MensajeCorreo.AppendFormat("<br />")
                    MensajeCorreo.AppendFormat("<table border=1 WIDTH=""800"" >")
                    MensajeCorreo.AppendFormat("   <tr>")
                    MensajeCorreo.AppendFormat("      <td style=""color:#0404B4""><b>CODoc</b></td>")
                    MensajeCorreo.AppendFormat("      <td style=""color:#0404B4""><b>TipoDoc</b></td>")
                    MensajeCorreo.AppendFormat("      <td style=""color:#0404B4""><b>NumDoc</b></td>")
                    MensajeCorreo.AppendFormat("      <td style=""color:#0404B4""><b>FechaDoc</b></td>")
                    MensajeCorreo.AppendFormat("      <td style=""color:#0404B4""><b>NitProveedor</b></td>")
                    MensajeCorreo.AppendFormat("      <td style=""color:#0404B4""><b>RazonSocialProv</b></td>")
                    MensajeCorreo.AppendFormat("      <td style=""color:#0404B4""><b>Valor</b></td>")
                    'MensajeCorreo.AppendFormat("      <td><b>EmailProv</b></td>")
                    MensajeCorreo.AppendFormat("      <td style=""color:#0404B4""><b>FacturaSiesa</b></td>")
                    MensajeCorreo.AppendFormat("      <td style=""color:#0404B4""><b>VendedorSiesa</b></td>")
                    MensajeCorreo.AppendFormat("   </tr>")
                    MensajeCorreo.AppendFormat("   <tr>")
                    MensajeCorreo.AppendFormat("      <td>{0}</td>", Row.Item(0).ToString)
                    MensajeCorreo.AppendFormat("      <td>{0}</td>", Row.Item(1).ToString)
                    MensajeCorreo.AppendFormat("      <td>{0}</td>", Row.Item(2).ToString)
                    MensajeCorreo.AppendFormat("      <td>{0}</td>", Row.Item(3).ToString)
                    MensajeCorreo.AppendFormat("      <td>{0}</td>", Row.Item(4).ToString)
                    MensajeCorreo.AppendFormat("      <td>{0}</td>", Row.Item(5).ToString)
                    MensajeCorreo.AppendFormat("      <td>{0}</td>", Row.Item(6).ToString)
                    'MensajeCorreo.AppendFormat("      <td>{0}</td>", Row.Item(7).ToString)
                    'MensajeCorreo.AppendFormat("      <td>{0}</td>", Row.Item(8).ToString)
                    MensajeCorreo.AppendFormat("      <td>{0}</td>", Row.Item(9).ToString)
                    MensajeCorreo.AppendFormat("      <td>{0}</td>", Row.Item(10).ToString)
                    MensajeCorreo.AppendFormat("   </tr>")
                    MensajeCorreo.AppendFormat("</table>")

                    MensajeCorreo.AppendFormat("<br />")
                    MensajeCorreo.AppendFormat("<table border=1 WIDTH=""800"" >")
                    MensajeCorreo.AppendFormat("   <tr>")
                    MensajeCorreo.AppendFormat("      <td style=""color:#0404B4""><b>Notas</b></td>")
                    MensajeCorreo.AppendFormat("   </tr>")
                    MensajeCorreo.AppendFormat("   <tr>")
                    MensajeCorreo.AppendFormat("      <td WIDTH=""800"" >{0}</td>", Row.Item(7).ToString)
                    MensajeCorreo.AppendFormat("   </tr>")
                    MensajeCorreo.AppendFormat("</table>")
                    Dim correos = Row.Item(8).ToString.Replace(";", ", ")
                    Dim envioCorreo = objCorreo.EnviarCorreoTarea("Notificación Ordenes de Compra", correos, MensajeCorreo.ToString)
                    'MsgBox("Envio bn")
                    If envioCorreo.Contains("Envio correcto") Then
                        objTarea.LogCorreoOC(Row.Item(0).ToString, Row.Item(1).ToString, Row.Item(2).ToString)
                    Else
                        AlmacenarLog(envioCorreo)
                    End If
                    AlmacenarLog("Envio correo OC: " + Row.Item(0).ToString + "-" + Row.Item(1).ToString + "-" + Row.Item(2).ToString)
                    'MsgBox("Almaceno bn")
                    MensajeCorreo.Clear()
                Next

            End If
        Catch ex As Exception
            AlmacenarLog(ex.Message)
        End Try




    End Sub

    Private Sub almacenarLogErrores(ByVal ErroresSiesa As String)
        Dim dsErroresSiesa As DataSet = New DataSet
        Dim ErrorSiesa As New StringBuilder

        If ErroresSiesa.Contains("<NewDataSet>") Then
            ErroresSiesa = ErroresSiesa.Replace("Error al importar el plano", "")
            ErroresSiesa = ErroresSiesa.Substring(0, ErroresSiesa.LastIndexOf(">") + 1)
            Dim xmlSR As System.IO.StringReader = New System.IO.StringReader(ErroresSiesa)
            dsErroresSiesa.ReadXml(xmlSR, XmlReadMode.Auto)

            For Each FilaError As System.Data.DataRow In dsErroresSiesa.Tables(0).Rows
                ErrorSiesa.Append("Linea:" & FilaError.Item(0).ToString & " Tipo de Registro:" & FilaError.Item(1).ToString & " SubTipo de resgistro:" & FilaError.Item(2).ToString & " Version:" & FilaError.Item(3).ToString & " Nivel" & FilaError.Item(4).ToString & " Error" & FilaError.Item(5).ToString & " Detalle:" & FilaError.Item(6).ToString & vbCrLf)
                ErrorSiesa.Append("<br />")
                objTarea.LogDetalleAlmacenar(FilaError.Item("f_nro_linea").ToString, FilaError.Item("f_tipo_reg").ToString, FilaError.Item("f_subtipo_reg").ToString, FilaError.Item("f_version").ToString, FilaError.Item("f_nivel").ToString, FilaError.Item("f_valor").ToString, FilaError.Item("f_detalle").ToString)
            Next
        End If
    End Sub

    Public Sub InvocarWebServiceUNOEE(ByVal Plano As String)

        Dim XMLCreacionTercero As String = ""
        Dim objUnoEE As New wsUnoEE.WSUNOEE

        'Valida el Check de consumo del web service en la tarea
        If objTarea.InvocarWebService Then
            Try
                XMLCreacionTercero &= "<?xml version='1.0' encoding='utf-8'?>"
                XMLCreacionTercero &= "<Importar>"
                XMLCreacionTercero &= "<NombreConexion>" & objTarea.NombreConexionWsSiesa & "</NombreConexion>"
                XMLCreacionTercero &= "<IdCia>" & objTarea.CiaWsSiesa & "</IdCia>"
                XMLCreacionTercero &= "<Usuario>" & objTarea.UsuarioWsSiesa & "</Usuario>"
                XMLCreacionTercero &= "<Clave>" & objTarea.ClaveWsSiesa & "</Clave> "
                XMLCreacionTercero &= Plano
                XMLCreacionTercero &= "</Importar>"

                'Se define un tiempo de espera de 15 minutos: 15 * 60 * 1000
                objUnoEE.Timeout = 900000

                Dim dsResultado As DataSet
                Dim UnoEEResultado As Short
                objTarea.LogFechaInicioWebServiceSiesa()
                dsResultado = objUnoEE.ImportarXML(XMLCreacionTercero, UnoEEResultado)

                If UnoEEResultado = 0 Then
                    objTarea.LogWebServiceSiesa(1)
                    objTarea.LogFechaFinWebServiceSiesa()
                Else
                    Dim ErrorSiesa As String = ""
                    For Each FilaError As System.Data.DataRow In dsResultado.Tables(0).Rows
                        ErrorSiesa &= "Fila:" & FilaError.Item(0).ToString & " Tipo de Registro:" & FilaError.Item(1).ToString & " SubTipo de resgistro:" & FilaError.Item(2).ToString & " Version:" & FilaError.Item(3).ToString & " Nivel" & FilaError.Item(4).ToString & " Valor" & FilaError.Item(5).ToString & " Detalle:" & FilaError.Item(6).ToString & vbCrLf
                    Next
                    objTarea.LogMensajesError(ErrorSiesa)
                    objTarea.LogWebServiceSiesa(0)
                    Exit Sub
                End If
            Catch ex As Exception
                objTarea.LogWebServiceSiesa(0)
                objTarea.LogMensajesError(ex.Message)
                Exit Sub
            End Try
        End If

    End Sub

    Private Sub AlmacenarXML(ByVal path As String, ByVal XML As String)
        Dim fs As FileStream = File.Create(path)
        Dim info As Byte() = New UTF8Encoding(True).GetBytes(XML)
        fs.Write(info, 0, info.Length)
        fs.Close()
    End Sub

    ''' <summary>
    ''' Valida si ocurrieron errores al importar los datos a Siesa Enterprise
    ''' </summary>
    ''' <returns> Valor booleano: True -> Ocurrieron errores False -> No ocurrieron errores</returns>
    ''' <remarks></remarks>
    Private Function ValidarErrores() As Boolean
        Dim dtResultado As DataTable = objTarea.ResultadoOperacion()

        If dtResultado.Rows(0).Item("Ejecución completa").ToString = "No" Then
            Return True
        ElseIf dtResultado.Rows(0).Item("Mensaje de error").ToString <> "" Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Sub EnviarNotificacion()

        Dim strMensaje As New StringBuilder
        Dim dtResultado As DataTable = objTarea.ResultadoOperacion()
        Dim strAsunto As String

        If dtResultado.Rows(0).Item("Mensaje de error").ToString = "" Then
            strAsunto = "GT Integration, Tarea: " & dtResultado.Rows(0).Item("Nombre").ToString & ", Completa: " & dtResultado.Rows(0).Item("Ejecución completa").ToString
        Else
            strAsunto = "GT Integration, Tarea: " & dtResultado.Rows(0).Item("Nombre").ToString & ", Completa: " & dtResultado.Rows(0).Item("Ejecución completa").ToString & ", con errores"
        End If

        strMensaje.AppendLine("<style type=""text/css"">")
        strMensaje.AppendLine("</style>")
        strMensaje.AppendLine("<BR/><BR/><table style=""color: #666; font-family: 'font-family: 'Georgia'', sans-serif;background-image:url('http://www.generictransfer.com/imagenes/main_bg.png')"" border=""0"" cellpadding=""0"" cellspacing=""0"" ")

        strMensaje.AppendLine("<tr>")
        strMensaje.AppendLine("<td style=""border-bottom-style: solid; border-bottom-width: medium; border-bottom-color: #2AA0D0"" bgcolor=""White"">")
        strMensaje.AppendLine("<img alt="""" src=""http://www.generictransfer.com/imagenes/logo_ge.png"" />")
        strMensaje.AppendLine("</td>")
        strMensaje.AppendLine("</tr>")

        strMensaje.AppendLine("<tr>")
        strMensaje.AppendLine("<td width=""700px"">")
        strMensaje.AppendLine("<br />")
        strMensaje.AppendLine("Generic Transfer Integration")
        strMensaje.AppendLine("</td>")
        strMensaje.AppendLine("</tr>")

        strMensaje.AppendLine("<tr>")
        strMensaje.AppendLine("<td style=""font-size: 18px;font-style: normal;font-weight: normal;font-variant: normal;text-decoration: none;color: #4799CC"">")
        strMensaje.AppendLine("<BR/>Información de la tarea " & dtResultado.Rows(0).Item("Id Tarea").ToString & " - " & dtResultado.Rows(0).Item("Nombre").ToString & " <BR/>")
        strMensaje.AppendLine("</td>")
        strMensaje.AppendLine("</tr>")

        strMensaje.AppendLine("<tr>")
        strMensaje.AppendLine("<td>")
        strMensaje.AppendLine("&nbsp;")
        strMensaje.AppendLine("</td>")
        strMensaje.AppendLine("</tr>")

        strMensaje.AppendLine("<tr>")
        strMensaje.AppendLine("<td>")
        If dtResultado.Rows(0).Item("Mensaje de error").ToString = "" Then
            strMensaje.AppendLine("Ejecución completa:" & dtResultado.Rows(0).Item("Ejecución completa").ToString)
        Else
            strMensaje.AppendLine("Ejecución completa:" & dtResultado.Rows(0).Item("Ejecución completa").ToString & ", con errores")
        End If
        strMensaje.AppendLine("</td>")
        strMensaje.AppendLine("</tr>")

        strMensaje.AppendLine("<tr>")
        strMensaje.AppendLine("<td>")
        strMensaje.AppendLine("Mensajes de Siesa Enterprise:" & dtResultado.Rows(0).Item("Mensaje de error").ToString)
        strMensaje.AppendLine("</td>")
        strMensaje.AppendLine("</tr>")

        strMensaje.AppendLine("<br />")

        strMensaje.AppendLine("<tr>")
        strMensaje.AppendLine("<td>")
        strMensaje.AppendLine("Inicio:" & dtResultado.Rows(0).Item("Fecha Inicio").ToString)
        strMensaje.AppendLine("</td>")
        strMensaje.AppendLine("</tr>")

        strMensaje.AppendLine("<tr>")
        strMensaje.AppendLine("<td>")
        strMensaje.AppendLine("Fin:" & dtResultado.Rows(0).Item("Fecha Fin").ToString)
        strMensaje.AppendLine("</td>")
        strMensaje.AppendLine("</tr>")

        strMensaje.AppendLine("<br />")

        strMensaje.AppendLine("<tr>")
        strMensaje.AppendLine("<td>")
        strMensaje.AppendLine("Inicio recuperación de datos Origen:" & dtResultado.Rows(0).Item("Fecha inicio de recuperacion de datos Origen").ToString)
        strMensaje.AppendLine("</td>")
        strMensaje.AppendLine("</tr>")

        strMensaje.AppendLine("<tr>")
        strMensaje.AppendLine("<td>")
        strMensaje.AppendLine("Fin recuperación de datos Origen:" & dtResultado.Rows(0).Item("Fecha fin de recuperacion de datos Origen").ToString)
        strMensaje.AppendLine("</td>")
        strMensaje.AppendLine("</tr>")

        strMensaje.AppendLine("<tr>")
        strMensaje.AppendLine("<td>")
        strMensaje.AppendLine("Recuperación de datos origen:" & dtResultado.Rows(0).Item("Recuperacion de datos origen").ToString)
        strMensaje.AppendLine("</td>")
        strMensaje.AppendLine("</tr>")
        strMensaje.AppendLine("<tr>")

        strMensaje.AppendLine("<br />")

        strMensaje.AppendLine("<tr>")
        strMensaje.AppendLine("<td>")
        strMensaje.AppendLine("Inicio de generación de plano:" & dtResultado.Rows(0).Item("Fecha inicio de generacion de plano").ToString)
        strMensaje.AppendLine("</td>")
        strMensaje.AppendLine("</tr>")

        strMensaje.AppendLine("<tr>")
        strMensaje.AppendLine("<td>")
        strMensaje.AppendLine("Fin de generación de plano:" & dtResultado.Rows(0).Item("Fecha fin de generacion de plano").ToString)
        strMensaje.AppendLine("</td>")
        strMensaje.AppendLine("</tr>")

        strMensaje.AppendLine("<tr>")
        strMensaje.AppendLine("<td>")
        strMensaje.AppendLine("Generación de plano:" & dtResultado.Rows(0).Item("Generacion de plano").ToString)
        strMensaje.AppendLine("</td>")
        strMensaje.AppendLine("</tr>")
        strMensaje.AppendLine("<tr>")


        If dtResultado.Rows(0).Item("InvocarWebService").ToString = "True" Then
            strMensaje.AppendLine("<br />")

            strMensaje.AppendLine("<tr>")
            strMensaje.AppendLine("<td>")
            strMensaje.AppendLine("Inicio Web Service:" & dtResultado.Rows(0).Item("Fecha inicio Web Service").ToString)
            strMensaje.AppendLine("</td>")
            strMensaje.AppendLine("</tr>")

            strMensaje.AppendLine("<tr>")
            strMensaje.AppendLine("<td>")
            strMensaje.AppendLine("Fin Web Service:" & dtResultado.Rows(0).Item("Fecha fin Web Service").ToString)
            strMensaje.AppendLine("</td>")
            strMensaje.AppendLine("</tr>")

            strMensaje.AppendLine("<tr>")
            strMensaje.AppendLine("<td>")
            strMensaje.AppendLine("Web Service de Siesa:" & dtResultado.Rows(0).Item("Consumir el Web Service de Siesa").ToString)
            strMensaje.AppendLine("</td>")
            strMensaje.AppendLine("</tr>")
        End If

        strMensaje.AppendLine("<br />")

        strMensaje.AppendLine("<tr>")
        strMensaje.AppendLine("<td align=""center"">")
        strMensaje.AppendLine("<strong><span class=""style2"">")
        strMensaje.AppendLine("<BR/><BR/>Interfaces y Soluciones S.A.S<br /> <BR/><BR/>")
        strMensaje.AppendLine("</span></strong>")
        strMensaje.AppendLine("</td>")
        strMensaje.AppendLine("</tr>")
        strMensaje.AppendLine("</table>")

        Dim objCorreo As New clsCorreo
        objCorreo.EnviarCorreoTarea(strAsunto, objTarea.CorreosNotificaciones, strMensaje.ToString)
    End Sub

    Public Sub EnvioCorreo()
        Dim datos As New DataSet
        'Dim config As New DataSet

        Try


            objTarea.SendMail(datos)

            Dim MensajeCorreo As New StringBuilder

            If datos.Tables(0).Rows.Count > 0 Then
                For Each Row As DataRow In datos.Tables(0).Rows
                    MensajeCorreo.AppendFormat("<h1>Notificación: Notas Créditos Aliados</h1>")
                    MensajeCorreo.AppendFormat("<br />")
                    MensajeCorreo.AppendFormat("<table border=1 >")
                    MensajeCorreo.AppendFormat("   <tr>")
                    MensajeCorreo.AppendFormat("      <td><b>NumDocNota</b></td>")
                    MensajeCorreo.AppendFormat("      <td><b>Factura</b></td>")
                    MensajeCorreo.AppendFormat("      <td><b>NitAliado</b></td>")
                    MensajeCorreo.AppendFormat("      <td><b>RazonSocialAliado</b></td>")
                    MensajeCorreo.AppendFormat("      <td><b>NitCliente</b></td>")
                    MensajeCorreo.AppendFormat("      <td><b>RazonSocialCliente</b></td>")
                    MensajeCorreo.AppendFormat("      <td><b>FechaNota</b></td>")
                    MensajeCorreo.AppendFormat("      <td><b>ValorNetoAliado</b></td>")
                    'MensajeCorreo.AppendFormat("      <td><b>ValorNeto</b></td>")
                    MensajeCorreo.AppendFormat("      <td><b>DescCausaDevol</b></td>")
                    MensajeCorreo.AppendFormat("      <td><b>Notas</b></td>")
                    MensajeCorreo.AppendFormat("   </tr>")

                    MensajeCorreo.AppendFormat("   <tr>")
                    MensajeCorreo.AppendFormat("      <td>{0}</td>", Row.Item(0).ToString)
                    MensajeCorreo.AppendFormat("      <td>{0}</td>", Row.Item(1).ToString)
                    MensajeCorreo.AppendFormat("      <td>{0}</td>", Row.Item(2).ToString)
                    MensajeCorreo.AppendFormat("      <td>{0}</td>", Row.Item(3).ToString)
                    MensajeCorreo.AppendFormat("      <td>{0}</td>", Row.Item(4).ToString)
                    MensajeCorreo.AppendFormat("      <td>{0}</td>", Row.Item(5).ToString)
                    MensajeCorreo.AppendFormat("      <td>{0}</td>", Row.Item(6).ToString)
                    MensajeCorreo.AppendFormat("      <td>{0}</td>", Row.Item(7).ToString)
                    MensajeCorreo.AppendFormat("      <td>{0}</td>", Row.Item(8).ToString)
                    MensajeCorreo.AppendFormat("      <td>{0}</td>", Row.Item(9).ToString)
                    'MensajeCorreo.AppendFormat("      <td>{0}</td>", Row.Item(10).ToString)
                    MensajeCorreo.AppendFormat("   </tr>")
                    MensajeCorreo.AppendFormat("</table>")
                    Dim correos = Row.Item(10).ToString.Replace(";", ", ")
                    Dim envioCorreo = objCorreo.EnviarCorreoTarea("Notificacion Notas", correos, MensajeCorreo.ToString)
                    MensajeCorreo = New StringBuilder
                    If envioCorreo.Contains("Envio correcto") Then
                        objTarea.LogCorreoNotas(Row.Item(1).ToString)
                    End If
                Next
            End If
        Catch ex As Exception


        End Try


    End Sub

    Public Sub AlmacenarLog(ByVal Mensaje As String)
        Dim file As System.IO.StreamWriter
        file = My.Computer.FileSystem.OpenTextFileWriter("C:\Users\Public\Documents\LogOC.txt", True)
        file.WriteLine(Mensaje)
        file.Close()
    End Sub

End Class
