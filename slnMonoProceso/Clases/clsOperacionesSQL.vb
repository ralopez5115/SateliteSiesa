﻿Imports System.Data.SqlClient

Public Class clsOperacionesSQL

    Public Sub sp_LimpiarTablasMicros()

        Try

            Dim sqlConexion As New SqlConnection(My.Settings.strConexionGT)
            Dim sqlComando As SqlCommand = New SqlCommand
            Dim sqlAdaptador As SqlDataAdapter = New SqlDataAdapter
            Dim dsResultado As New DataSet

            sqlComando.Connection = sqlConexion
            sqlComando.CommandType = CommandType.StoredProcedure
            sqlComando.CommandText = "sp_LimpiarTablasMicros"
            sqlAdaptador.SelectCommand = sqlComando
            'sqlComando.Parameters.AddWithValue("@idTarea", _Tarea)

            sqlAdaptador.SelectCommand = sqlComando

            sqlConexion.Open()
            sqlComando.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        End Try

    End Sub


    Public Sub Almacenar_Micros_fcr_customer_data(ByVal Datos As DataTable)

        Dim sqlConexion As New SqlConnection(My.Settings.strConexionGT)

        Try
            sqlConexion.Open()

            Using bulkCopy As SqlBulkCopy = New SqlBulkCopy(sqlConexion)
                bulkCopy.DestinationTableName = "dbo.Micros_fcr_customer_data"
                bulkCopy.WriteToServer(Datos)
            End Using

        Catch ex As Exception
            Throw ex
        Finally
            sqlConexion.Close()
        End Try

    End Sub

    Public Sub Almacenar_Micros_fcr_invoice_data(ByVal Datos As DataTable)

        Dim sqlConexion As New SqlConnection(My.Settings.strConexionGT)

        Try
            sqlConexion.Open()

            Using bulkCopy As SqlBulkCopy = New SqlBulkCopy(sqlConexion)
                bulkCopy.DestinationTableName = "dbo.Micros_fcr_invoice_data"
                bulkCopy.WriteToServer(Datos)
            End Using

        Catch ex As Exception
            Throw ex
        Finally
            sqlConexion.Close()
        End Try

    End Sub


    Public Sub Almacenar_Micros_fcr_invoice_data_ext(ByVal Datos As DataTable)

        Dim sqlConexion As New SqlConnection(My.Settings.strConexionGT)

        Try
            sqlConexion.Open()

            Using bulkCopy As SqlBulkCopy = New SqlBulkCopy(sqlConexion)
                bulkCopy.DestinationTableName = "dbo.Micros_fcr_invoice_data_ext"
                bulkCopy.WriteToServer(Datos)
            End Using

        Catch ex As Exception
            Throw ex
        Finally
            sqlConexion.Close()
        End Try

    End Sub

    Public Sub Almacenar_Micros_fcr_invoice_print(ByVal Datos As DataTable)

        Dim sqlConexion As New SqlConnection(My.Settings.strConexionGT)

        Try
            sqlConexion.Open()

            Using bulkCopy As SqlBulkCopy = New SqlBulkCopy(sqlConexion)
                bulkCopy.DestinationTableName = "dbo.Micros_fcr_invoice_print"
                bulkCopy.WriteToServer(Datos)
            End Using

        Catch ex As Exception
            Throw ex
        Finally
            sqlConexion.Close()
        End Try

    End Sub

    Public Sub Almacenar_Micros_fcr_ttl_data(ByVal Datos As DataTable)

        Dim sqlConexion As New SqlConnection(My.Settings.strConexionGT)

        Try
            sqlConexion.Open()

            Using bulkCopy As SqlBulkCopy = New SqlBulkCopy(sqlConexion)
                bulkCopy.DestinationTableName = "dbo.Micros_fcr_ttl_data"
                bulkCopy.WriteToServer(Datos)
            End Using

        Catch ex As Exception
            Throw ex
        Finally
            sqlConexion.Close()
        End Try

    End Sub

    Public Sub sp_MERGE_ejecucion(conector As String)

        Try

            Dim sqlConexion As New SqlConnection(My.Settings.strConexionGT)
            Dim sqlComando As SqlCommand = New SqlCommand
            Dim sqlAdaptador As SqlDataAdapter = New SqlDataAdapter
            Dim dsResultado As New DataSet

            sqlComando.Connection = sqlConexion
            sqlComando.CommandType = CommandType.StoredProcedure
            sqlComando.CommandText = "sp_MERGE_ejecucion"
            sqlAdaptador.SelectCommand = sqlComando
            sqlComando.Parameters.AddWithValue("@conector", conector)

            sqlAdaptador.SelectCommand = sqlComando

            sqlConexion.Open()
            sqlComando.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        End Try

    End Sub



    Public Sub sp_MERGE_validacion(campoclave As String)

        Try

            Dim sqlConexion As New SqlConnection(My.Settings.strConexionGT)
            Dim sqlComando As SqlCommand = New SqlCommand
            Dim sqlAdaptador As SqlDataAdapter = New SqlDataAdapter
            Dim dsResultado As New DataSet

            sqlComando.Connection = sqlConexion
            sqlComando.CommandType = CommandType.StoredProcedure
            sqlComando.CommandText = "sp_MERGE_validacion"
            sqlAdaptador.SelectCommand = sqlComando
            sqlComando.Parameters.AddWithValue("@dato", campoclave)

            sqlAdaptador.SelectCommand = sqlComando

            sqlConexion.Open()
            sqlComando.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        End Try

    End Sub




End Class
